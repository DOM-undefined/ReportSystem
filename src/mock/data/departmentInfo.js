/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2018-01-03 14:23:12
 * @version $Id$
 */

const Mock = require('mockjs');

let deptData = [];

for (var i = 0; i <= Mock.Random.integer(2, 10) - 1; i++){
	deptData.push({
		id: Mock.Random.integer(10, 100000),
		ownerDept: Mock.Random.word(3, 12),
		ownerDeptId: Mock.Random.integer(10, 100000),
		lookupDept: Mock.Random.word(3, 12),
		lookupDeptId: Mock.Random.integer(10, 100000),
		// 辅助字段
		isedit: false,
		// 辅助字段
		visible: false
		// dsporder: Mock.Random.integer(0, 100)
	});
}

module.exports = deptData;