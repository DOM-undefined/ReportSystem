/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2018-01-04 09:55:06
 * @version $Id$
 */

const Mock = require('mockjs');

let hrmAuthorityData = [];

for (var i = 0; i <= Mock.Random.integer(2, 10) - 1; i++){
	hrmAuthorityData.push({
		id: Mock.Random.integer(10, 100000),
		hrmid: Mock.Random.integer(10, 100000),
		name: Mock.Random.ctitle(),
		dept: Mock.Random.ctitle() + ',' + Mock.Random.ctitle(),
		deptId: Mock.Random.integer(10, 100000) + ',' + Mock.Random.integer(10, 100000),
		authorityState: Mock.Random.boolean(),
		// 辅助字段
		isedit: false,
		// 辅助字段
		visible: false
		// dsporder: Mock.Random.integer(0, 100)
	});
}

module.exports = hrmAuthorityData;