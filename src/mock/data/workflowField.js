/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2018-01-04 13:30:51
 * @version $Id$
 */

const Mock = require('mockjs');

let workflowFieldData = [];

for (var i = 0; i <= Mock.Random.integer(2, 10) - 1; i++){
	workflowFieldData.push({
		id: Mock.Random.integer(10, 100000),
		label: Mock.Random.ctitle(),
	});
}

module.exports = workflowFieldData;