/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2018-01-04 09:54:35
 * @version $Id$
 */

const Mock = require('mockjs');

let headerData = [];

for (var i = 0; i <= Mock.Random.integer(2, 10) - 1; i++){
	headerData.push({
		id: Mock.Random.integer(10, 100000),
		ownerDept: Mock.Random.ctitle(),
		ownerDeptId: Mock.Random.integer(10, 100000),
		lookupDept: Mock.Random.ctitle() + ',' + Mock.Random.ctitle(),
		lookupDeptId: Mock.Random.integer(10, 100000) + ',' + Mock.Random.integer(10, 100000),
		authorityState: Mock.Random.boolean(),
		// 辅助字段
		isedit: false,
		// 辅助字段
		visible: false
		// dsporder: Mock.Random.integer(0, 100)
	});
}

module.exports = headerData;