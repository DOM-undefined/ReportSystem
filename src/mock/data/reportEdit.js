/**
 * 
 * @authors Jungaung li (you@example.org)
 * @date    2017-11-06 15:18:42
 * @version $Id$
 */

 const Mock = require('mockjs')

 let workflowData = {total: Mock.Random.integer(10, 200), rows: []};

 let type = ['success', 'info', 'warning', 'danger'];

 function getType(){
 	return Mock.mock({'rs|1': type}).rs;
 }
 /**
  * [getWorkflowData 获得流程配置数据]
  * @param  {[type]} num [生成个数]
  * @return {[type]}     [json]
  */
 function getWorkflowData(num){
 	num = num || Mock.Random.integer(10, 50);
 	workflowData.rows = [];
 	for (let i = 0; i <= num - 1; i++){
	 	workflowData.rows.push({
	 		id: Mock.Random.integer(10, 10000),
	 		reportId: Mock.Random.integer(10, 10000),
	 		reportName: Mock.Random.ctitle(),
	 		workflowId: Mock.Random.integer(10, 10000),
	 		workflowName: Mock.Random.ctitle(),
	 		authId: Mock.Random.integer(10, 10000),
	 		authName: Mock.Random.ctitle(),
	 		authModel: Mock.Random.integer(0, 3),
	 		authType: getType(),
	 		visible: false 	
	 	});
	}
	return  workflowData;
 }


module.exports = getWorkflowData;
