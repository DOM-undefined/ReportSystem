/**
 * 
 * @authors Jungaung li (you@example.org)
 * @date    2017-11-06 15:18:42
 * @version $Id$
 */

const Mock = require('mockjs');

let deptData = [];

function geDeptData(){
	for (var i = 0; i <= Mock.Random.integer(2, 10) - 1; i++){
		deptData.push({
			id: Mock.Random.integer(10, 100000),
			label: Mock.Random.ctitle(),
			deptId: Mock.Random.integer(10, 100000),
			leaf: Mock.Random.boolean(),
			// 辅助字段
			isedit: false,
			// 辅助字段
			visible: false
			// dsporder: Mock.Random.integer(0, 100)
		});
	}

	return deptData;
}

module.exports = geDeptData;
