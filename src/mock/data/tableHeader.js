/**
 * 
 * @authors Junguang li (you@example.org)
 * @date    2017-11-08 09:18:08
 * @version $Id$
 */

const Mock = require('mockjs');

let headerData = [];

for (var i = 0; i <= Mock.Random.integer(10, 200) - 1; i++){
	headerData.push({
		id: Mock.Random.integer(10, 100000),
		reportId: Mock.Random.integer(10, 10000),
		fieldId: Mock.Random.integer(10, 100000),
		fieldName: Mock.Random.word(3, 12),
		labelId: Mock.Random.integer(10, 100000),
		label: Mock.Random.ctitle(),
		width: '',
		align: Mock.mock({'align|1': ['left', 'center', 'right']}).align,
		sortable: Mock.mock({'sortable|1': [true, false]}).sortable,
		display: Mock.Random.integer(0, 1),
		// 辅助字段
		isedit: false,
		// 辅助字段
		visible: false
		// dsporder: Mock.Random.integer(0, 100)
	});
}

module.exports = headerData;