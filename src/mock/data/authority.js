/**
 * 
 * @authors Jungaung Li (you@example.org)
 * @date    2017-11-14 09:43:51
 * @version $Id$
 */

 const Mock = require('mockjs')

 let authorityData = {total: Mock.Random.integer(10, 200), rows: []};

 let type = ['success', 'info', 'warning', 'danger'];

 function getType(){
 	return Mock.mock({'rs|1': type}).rs;
 }
 /**
  * [getWorkflowData 获得流程配置数据]
  * @param  {[type]} num [生成个数]
  * @return {[type]}     [json]
  */
 function getAuthorityData(num){
 	num = num || Mock.Random.integer(10, 50);
 	authorityData.rows = [];
 	for (let i = 0; i <= num - 1; i++){
	 	authorityData.rows.push({
	 		id: Mock.Random.integer(10, 10000),
	 		name: Mock.Random.ctitle(),
	 		workflowReportId: Mock.Random.integer(10, 10000),
	 		workflowReportName: Mock.Random.ctitle(),
	 		deptFieldId: Mock.Random.integer(10, 10000),
	 		deptFieldLabel: Mock.Random.ctitle(),
	 		requestFieldId: Mock.Random.integer(10, 10000),
	 		requestFieldLabel: Mock.Random.ctitle(),
	 		visible: false 	
	 	});
	}
	return  authorityData;
 }


module.exports = getAuthorityData;
