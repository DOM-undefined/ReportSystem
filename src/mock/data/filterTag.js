/**
 * 
 * @authors Junguang Li (you@example.org)
 * @date    2017-11-14 11:26:37
 * @version $Id$
 */


const Mock = require('mockjs');

let filterTagData = [];

for (var i = 0; i <= Mock.Random.integer(5, 10) - 1; i++){
	filterTagData.push({
		text: Mock.Random.ctitle(),
		value: Mock.Random.integer(10, 10000),

	});
}

module.exports = filterTagData;