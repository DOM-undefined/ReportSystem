/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2018-01-05 14:44:56
 * @version $Id$
 */

const Mock = require('mockjs');

let remoteData = [];

for (var i = 0; i <= Mock.Random.integer(2, 10) - 1; i++){
	remoteData.push({
		id: Mock.Random.integer(10, 100000),
		lastname: Mock.Random.ctitle(),
		jobtitle: Mock.Random.ctitle()
		// dsporder: Mock.Random.integer(0, 100)
	});
}

module.exports = remoteData;