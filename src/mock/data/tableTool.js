/**
 * 
 * @authors Junguang Li (you@example.org)
 * @date    2017-11-11 14:16:48
 * @version $Id$
 */

const Mock = require('mockjs');

let toolData = [];

for (var i = 0; i <= Mock.Random.integer(2, 10) - 1; i++){
	toolData.push({
		id: Mock.Random.integer(10, 100000),
		reportId: Mock.Random.integer(10, 10000),
		fieldId: Mock.Random.integer(10, 100000),
		fieldName: Mock.Random.word(3, 12),
		label: Mock.Random.ctitle(),
		type: Mock.mock({'type|1': ['rule-input', 'rule-number', 'rule-select', 'rule-datepicker']}).type,
		// 辅助字段
		isedit: false,
		// 辅助字段
		visible: false
		// dsporder: Mock.Random.integer(0, 100)
	});
}

module.exports = toolData;