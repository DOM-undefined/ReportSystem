/**
 * 
 * @authors junguang Li (you@example.org)
 * @date    2017-10-25 11:44:12
 * @version $Id$
 */

const express = require('express');
const router = express.Router();
const Mock = require('mockjs');
var Random = Mock.Random;

var reportData = require('./data/reportEdit.js');
var headerData = require('./data/tableHeader.js');
var toolData = require('./data/tableTool.js');
var authorityData = require('./data/authority.js');
var filterTag = require('./data/filterTag.js');
var treeDept = require('./data/treeDept.js');
var hrmAuthority = require('./data/hummerAuthority.js');
var deptAuthority = require('./data/departmentAuthority.js');
var workflowReport = require('./data/workflowReport.js');
var workflowField = require('./data/workflowField.js');
var hummerRemoteData = require('./data/hummerRemoteData.js');

//json数据
var menu = require('../../static/json/menu.json');
var hrm = require('../../static/json/hrm.json');
var deptTree = require('../../static/json/dept.json');
var menuTree = require('../../static/json/linkMenu.json');

// menu数据
router.post('/data/menu', (req, res) => {
	res.json(menu);
});

// 人员数据
router.post('/hrm/query', (req, res) => {
    res.json(hrm);
});

// 报表配置数据
router.post('/report/query', (req, res) => {
	res.json(reportData(req.body.rows));
});

// 报表字段数据
router.post('/report/field', (req, res) => {
	res.json(headerData);
});

// 报表表单规则数据
router.post('/report/tool', (req, res) => {
	res.json(toolData);
})

// 报表权限数据
router.post('/report/authority', (req, res) => {
	res.json(authorityData(req.body.rows));
});

// 报表权限过滤数据
router.post('/report/filterTag', (req, res) => {
	res.json(filterTag);
});

// 部门结构数据
router.post('/tree/dept', (req, res) => {
	res.json(treeDept());
});

// 部门结构数据
router.post('/tree/deptData', (req, res) => {
	res.json(deptTree);
});

// 菜单结构数据
router.post('/tree/menu', (req, res) => {
	res.json(menuTree);
});

// 特殊人员权限数据
router.post('/authority/hummer', (req, res) => {
	res.json(hrmAuthority);
});

// 流程报表信息
router.post('/report/authority/select/workflowReport', (req, res) => {
	res.json(workflowReport);
});

// 流程字段信息
router.post('/report/authority/select/workflowField', (req, res) => {
	res.json(workflowField);
});

// 部门权限数据
router.post('/authority/department', (req, res) =>{
	res.json(deptAuthority);
});

// 人员信息远程搜索
router.post('/authority/hummer/remoteHummer', (req, res) => {
	res.json(hummerRemoteData);
});

module.exports = router;