
import home from './components/Home.vue'

import hrm from './page/HRM/HRResource.vue'

import reportEditor from './page/report/reportEditor.vue'
import reportAuthority from './page/report/reportAuthority.vue'
import reportPreview from './page/report/reportPreview.vue'


import reportModel from './components/reportModel.vue'

export default [
  {
    path: '/', component: home, props: true,
    children:[
      {
        path: 'HRMChange', component: hrm, props: true,
        children:[]
      },{
        path:'flowReportEditor', component: reportEditor, props: true,
        children:[]
      },{
        path: 'flowReportPreview', component: reportPreview, props: true,
        children:[
          {name:'flowReportPreview', path: ':reportId', component: reportModel, props: true,}
        ]
      },{
        path:'flowReportAuthority', component: reportAuthority, props: true,
        children:[]
      },
      {name:'flowReport', path: 'report/:reportId', component: reportModel, props: true,}
    ]
  },
]
